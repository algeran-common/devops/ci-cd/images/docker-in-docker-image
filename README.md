### Docker-in-docker image for Gitlab CI/CD

Example of creating image with Docker-in-docker and docker-credential-helper-pass with installed docker credentials to docker_registry.  
Helps to avoid adding `docker login -u <username> -p <password> <docker_registry>` every time you need docker in jobs

[Steps to reproduce all requirements for build](docs/docker-credential-helper-pass.md)
