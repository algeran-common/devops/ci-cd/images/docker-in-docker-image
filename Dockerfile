# ========= builder =========
ARG BASE_ALPINE_IMAGE

FROM    debian:10.5 as builder

ARG     OPENSSH_CLIENT_VERSION
ARG     GIT_VERSION
ARG     WGET_VERSION
ARG     GPG_VERSION

RUN     mkdir workdir \
            && mkdir /workdir/.password-store \
            && mkdir /workdir/docker

WORKDIR /workdir


#### START Gitlab SSH connection and clone pass store repo #####

COPY    gitlab-key          ./gitlab_key

RUN     apt-get -y update \
            && apt-get install -y openssh-client=${OPENSSH_CLIENT_VERSION} \
            && apt-get install -y git=${GIT_VERSION} \
            && mkdir -m 700 ${HOME}/.ssh \
            && mv ./gitlab_key ${HOME}/.ssh/gitlab_key \
            && chmod 600 ${HOME}/.ssh/gitlab_key \
            && eval $(ssh-agent -s) \
            && ssh-add ${HOME}/.ssh/gitlab_key \
            && ssh-keyscan -t rsa <gitlab_host> >> ${HOME}/.ssh/known_hosts \
            && git config --global user.email <gitlab_user_email> \
            && git config --global user.name <gitlab_user_name> \
            && git clone <pass_storage_gitlab_repository.git> \
            && rm -rf pass-docker-secret-store/.git \
            && mv pass-docker-secret-store/* /workdir/.password-store

#### END Gitlab SSH connection and clone pass store repo #####
### START Download docker-credential-helper-pass and docker config

RUN     apt-get -y update \
            && apt-get install -y wget=${WGET_VERSION} \
            && wget https://github.com/docker/docker-credential-helpers/releases/download/v0.6.3/docker-credential-pass-v0.6.3-amd64.tar.gz \
            && tar xvzf docker-credential-pass-v0.6.3-amd64.tar.gz \
            && mv docker-credential-pass /workdir/docker/ \
            && mkdir /workdir/docker/.docker \
            && echo '{ "credsStore": "pass" }' > /workdir/docker/.docker/config.json

### END Download docker-credential-helper-pass
### START add keys to GPG

COPY    gpg-secret.key              ./gpg-secret.key
COPY    gpg-ownertrust.txt          ./gpg-ownertrust.txt

RUN     apt-get -y update \
            && apt-get install -y gpg=${GPG_VERSION} \
            && gpg --allow-secret-key-import --batch --import ./gpg-secret.key  \
            && gpg --import-ownertrust ./gpg-ownertrust.txt \
            && cp -r /root/.gnupg /workdir/

### END add keys to GPG

##############################
##############################

# ========= final_image =========

FROM    ${BASE_ALPINE_IMAGE}

ARG     PASS_VERSION
ARG     GNUPG_VERSION

COPY    --from=builder /workdir/.password-store                 /root/.password-store
COPY    --from=builder /workdir/docker/docker-credential-pass   /usr/bin/docker-credential-pass
COPY    --from=builder /workdir/docker/.docker                  /root/.docker
COPY    --from=builder /workdir/.gnupg                          /root/.gnupg

        ### install packages
RUN     apk update \
            && apk add pass=${PASS_VERSION} \
            && apk add gnupg=${GNUPG_VERSION} \
        ### configure permissions
            && chmod a+x /usr/bin/docker-credential-pass \
            && chmod 700 ~/.gnupg/ \
            && chmod -R go-rwx ~/.password-store \
            && chmod 600 ~/.docker/config.json
